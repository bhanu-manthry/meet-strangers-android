package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FirebaseLib {

    Context context;
    private static final String TAG = FirebaseLib.class.getSimpleName();
    private static FirebaseLib instance = null;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference onlineUsersRef = database.getReference("onlineUsers");

    ValueEventListener valueEventListener;

    private AppState appState;

    boolean strangerFound = false;

    private FirebaseLib(Context context) {
        this.context = context;
        appState = AppState.getInstance();


    }

    public static FirebaseLib getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseLib(context);
        }

        return instance;
    }

    public void newUser() {
        onlineUsersRef.child(appState.getUserId()).setValue(appState);
    }

    public void destroyUser(String userId) {
        onlineUsersRef.child(userId).removeValue();
    }

    public void updateUserState(AppState appState) {
        onlineUsersRef.child(appState.getUserId()).setValue(appState);
    }

    public void userTryingToConnect() {
        String userId = appState.getUserId();
        appState.setConnectingToSomebody(true);
        getAllOnline(this::sendPingReqToAllOnlineTopics);
        onlineUsersRef.child(userId).setValue(appState);
    }

    public void stopConnecting() {
        String userId = appState.getUserId();
        appState.setConnectingToSomebody(false);
        onlineUsersRef.child(userId).setValue(appState);
    }

    private void sendPingReqToAllOnlineTopics(ArrayList<String> allOnlineTopics) {

        Log.d(TAG, "allOnlineTopics size: " + allOnlineTopics.size());

        for(String onlineTopic : allOnlineTopics) {
            if (strangerFound) {
                Log.d(TAG, "Stranger found while sending ping requests");
                break;
            }

            Log.d(TAG, "sending ping req");
            SendMessage.getInstance(context).sendPingReq(onlineTopic);
        }

        Log.d(TAG, "Did not find any stranger while sending ping requests");
    }

    private void getAllOnline(Cb consumer) {
        ArrayList<String> allOnlineTopics = new ArrayList<>();
        String userId = appState.getUserId();
        // search for other user who is trying to connect
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onDataChange called");
                for (DataSnapshot itemSnapshot : dataSnapshot.getChildren()) {
                    if (!itemSnapshot.getKey().equals(userId)) {
                        boolean connected = (boolean) itemSnapshot.child("connectedToSomebody").getValue();
                        boolean connecting = (boolean) itemSnapshot.child("connectingToSomebody").getValue();
                        Log.d(TAG, "connected: " + connected);
                        Log.d(TAG, "connecting: " + connecting);
                        if (!connected && connecting) {
                            Log.d(TAG, "in if condition");
                            String strangerTopic = itemSnapshot.child("userTopic").getValue().toString();
                            allOnlineTopics.add(strangerTopic);
                        }
                    }
                }

                consumer.invoke(allOnlineTopics);
                removeValueEventListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };

        onlineUsersRef.addValueEventListener(valueEventListener);
    }

    private void removeValueEventListener() {
        onlineUsersRef.removeEventListener(valueEventListener);
    }

    interface Cb {
        void invoke(ArrayList<String> strings);
    }
}
