package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bhanu.meetstrangers.beans.ChatMessage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by bhanu on 31/10/17.
 */

public class MessagesAdapter extends BaseAdapter {

    private static final String TAG = MessagesAdapter.class.getSimpleName();
    private ArrayList<ChatMessage> messages;
    private LayoutInflater inflater;
    Context context;

    MessagesAdapter(Context context) {
        this.messages = ChatMessages.getInstance().getMessages();
        this.context = context;
        inflater = (LayoutInflater.from(context));
    }

    public void newMessage(ChatMessage chatMessage) {
        this.messages.add(chatMessage);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        Log.d(TAG, "Size: " + messages.size());
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        ChatMessage chatMessage = messages.get(position);
        ChatMessage.Message message = chatMessage.getMessage();

        if (message.getType().equals("sent")) {
            view = inflater.inflate(R.layout.sent_message_view, parent, false);
        }

        else if (message.getType().equals("received")) {
            view = inflater.inflate(R.layout.received_message_view, parent, false);
        }

        ((TextView) view.findViewById(R.id.message_text))
                .setText(message.getMsgText());

//        ImageView msgImage = view.findViewById(R.id.msg_image);
//        Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(msgImage);

        return view;
    }
}
