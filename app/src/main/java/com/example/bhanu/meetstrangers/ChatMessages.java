package com.example.bhanu.meetstrangers;

import com.example.bhanu.meetstrangers.beans.ChatMessage;

import java.util.ArrayList;

/**
 * Created by bhanu on 1/11/17.
 */

public class ChatMessages {
    private static ChatMessages instance = null;

    private ArrayList<ChatMessage> messages = null;

    private ChatMessages() {
        messages = new ArrayList<>();
    }

    public static ChatMessages getInstance() {
        if (instance == null) {
            instance = new ChatMessages();
        }

        return instance;
    }

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }

    public void deleteAllMessages() {
        messages = new ArrayList<>();
    }
}
