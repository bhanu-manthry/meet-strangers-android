package com.example.bhanu.meetstrangers.beans;

/**
 * Created by bhanu on 31/10/17.
 */

public class ChatMessage implements Cloneable {

    public class Message {
        String msgText = null,
                img = null,
                type = null;

        public Message(String msgText, String img, String type) {
            this.msgText = msgText;
            this.img = img;
            this.type = type;
        }

        public String getMsgText() {
            return msgText;
        }

        public void setMsgText(String msgText) {
            this.msgText = msgText;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    Message message;
    String toId, fromId;

    public ChatMessage(String msgText, String img, String type, String toId, String fromId) {
        message = new Message(msgText, img, type);
        this.toId = toId;
        this.fromId = fromId;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    @Override
    public ChatMessage clone() {
        try {
            return (ChatMessage) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }
}