package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cloudinary.android.MediaManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference onlineUsersRef;

    private ProgressBar progressBar;
    private TextView connectingTextView;

    AppState appState;

    static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent serviceIntent = new Intent(getApplicationContext(), MSService.class);
        startService(serviceIntent);

        progressBar = findViewById(R.id.progressBar);
        connectingTextView = findViewById(R.id.connectingTextView);

        String userId;
        appState = AppState.getInstance();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);

        userId = sharedPref.getString("userId", null);

        if (userId == null) {
            SharedPreferences.Editor editor = sharedPref.edit();
            userId = UUID.randomUUID().toString();
            editor.putString("userId", userId);
            editor.apply();
        }

        appState.setUserId(userId);

        isInternetAvailable();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    progressBar.setVisibility(View.INVISIBLE);
                    connectingTextView.setText("No internet!");
                    connectingTextView.setTextColor(Color.RED);
                    Thread t = new Thread(() -> {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        isInternetAvailable();
                    });

                    t.start();
                    return;
                }

                connectingTextView.setText("Connecting");
                progressBar.setVisibility(View.VISIBLE);
                initialCheck();
            }
        };
    }

    public void initialCheck() {
        connectingTextView.setTextColor(Color.rgb(245,124,0));
        DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.d(TAG, "connected to firebase");
                    onlineUsersRef = database.getReference("onlineUsers");
                    onlineUsersRef.child(appState.getUserId());
                    onlineUsersRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            onlineUsersRef.child(appState.getUserId()).setValue(appState);
                        }

                        @Override
                        public void onCancelled(DatabaseError firebaseError) {
                        }
                    });

                    MqttWorker mqttWorker = MqttWorker.getInstance(getApplicationContext());
                    mqttWorker.subscriptionTopics.add(AppState.getInstance().getUserTopic());
                    mqttWorker.subscriptionTopics.add(AppState.CLIENT_TOPIC);

                    connectingTextView.setTextColor(Color.rgb(56,142,60));

                    mqttWorker.connect((c) -> {
                        if (!c) {
                            Log.e(TAG, "Cannot connect to mqtt broker due to unknown error");
                            connectingTextView.setTextColor(Color.rgb(211,47,47));
                            return;
                        }

                        connectingTextView.setTextColor(Color.rgb(25,118,210));

                        Log.d(TAG, "successfully connected to mqtt broker");
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);

                        startActivity(intent);
                        finish();
                    });

                } else {
                    Log.d(TAG, "not connected to firebase");
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e(TAG, "Listener was cancelled");
            }
        });
    }

    public void isInternetAvailable() {
        Log.d(TAG, "Checking for internet connection");
        Thread t = new Thread(() -> {
            boolean available;
            try {
                final InetAddress address = InetAddress.getByName("www.google.com");
                available = !address.equals("");
                handler.sendEmptyMessage(available ? 1 : 0);
                return;
            } catch (UnknownHostException e) {
                // Log error
            }

            available = false;
            handler.sendEmptyMessage(available ? 1 : 0);
        });

        t.start();
    }

    interface AfterMqttConnect {
        void callback(boolean connected);
    }
}
