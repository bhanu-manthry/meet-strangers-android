package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.bhanu.meetstrangers.beans.ActionTypes;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.eclipse.paho.client.mqttv3.MqttException;

public class HomeActivity extends AppCompatActivity implements MessagesListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    Button connectBtn, stopBtn;
    LinearLayout connectingStatusContent, stopContentLayout;
    MediaPlayer mediaPlayer;
    Vibrator vibe;

    private DatabaseReference onlineUsersRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        onlineUsersRef = FirebaseDatabase.getInstance().getReference("onlineUsers");

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Meet Strangers");
        setSupportActionBar(toolbar);

        vibe = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.awareness);

        MqttWorker.getInstance(getApplicationContext()).setListener(this);
        initializeControls();

        connectingStatusContent.setVisibility(View.GONE);
        connectBtn.setOnClickListener(view -> {
            Log.d(TAG, "connect button pressed");
            stopContentLayout.setVisibility(View.VISIBLE);
            connectBtn.setVisibility(View.GONE);
            connectingStatusContent.setVisibility(View.VISIBLE);
            FirebaseLib.getInstance(getApplicationContext()).userTryingToConnect();
        });

        stopBtn.setOnClickListener((View view) -> {
            stopContentLayout.setVisibility(View.GONE);
            connectBtn.setVisibility(View.VISIBLE);
            connectingStatusContent.setVisibility(View.VISIBLE);
            connectingStatusContent.setVisibility(View.GONE);
            FirebaseLib.getInstance(getApplicationContext()).stopConnecting();
        });
    }

    @Override
    public void onMessageArrival(String msgStr) {
        Log.d(TAG, "onMessageArrival: " + msgStr);
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(msgStr).getAsJsonObject();
        String type = jsonObject.get("notification_type").getAsString();

        Log.d(TAG, type);

        if (type.equals(ActionTypes.CONNECT_PING_REQ)) {
            Log.d(TAG, "received connect ping req");
            String strangerTopic = jsonObject.get("strangerTopic").getAsString();

            AppState appState = AppState.getInstance();
            appState.setConnectedToSomebody(true);
            appState.setConnectingToSomebody(false);
            appState.setStrangerTopic(strangerTopic);

            onlineUsersRef.child(appState.getUserId()).setValue(appState);
            SendMessage.getInstance(getApplicationContext()).sendPingAck(appState.getStrangerTopic());

            gotoChatActivity();
        }

        if (type.equals(ActionTypes.CONNECT_PING_ACK)) {
            Log.d(TAG, "received connect ping ack");
            String strangerTopic = jsonObject.get("strangerTopic").getAsString();

            AppState appState = AppState.getInstance();
            appState.setConnectedToSomebody(true);
            appState.setConnectingToSomebody(false);
            appState.setStrangerTopic(strangerTopic);

            onlineUsersRef.child(appState.getUserId()).setValue(appState);

            gotoChatActivity();
        }
    }

    private void gotoChatActivity() {
        mediaPlayer.start();
        vibe.vibrate(200);
        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
        startActivity(intent);
        finish();
    }

    private void initializeControls() {
        connectBtn = findViewById(R.id.connectBtn);
        stopBtn = findViewById(R.id.stopBtn);
        connectingStatusContent = findViewById(R.id.connectingStatusContent);
        stopContentLayout = findViewById(R.id.stopContentLayout);
    }
}
