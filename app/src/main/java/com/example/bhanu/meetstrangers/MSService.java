package com.example.bhanu.meetstrangers;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by bhanu on 6/11/17.
 */

public class MSService extends Service {
    private static final String TAG = MSService.class.getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        Thread t = new Thread(() -> {
            int i = 0;
            while(true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "from service " + ++i);
            }

        });

        t.start();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "app closed");
    }
}
