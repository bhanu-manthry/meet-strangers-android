package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bhanu.meetstrangers.beans.ChatMessage;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLongArray;

/**
 * Created by bhanu on 30/10/17.
 */

public class AppState implements Cloneable {

    private static AppState instance = null;

    public static final String CLIENT_TOPIC = "/meet_strangers/client/";
    public static final String CLIENT_STATUS_TOPIC = "/meet_strangers/server/client_status";

    private boolean connectedToSomebody = false,
            connectingToSomebody = false,
            strangerDisconnected = false,
            strangerIsTyping = false,
            userDisconnected = false;

    private String strangerTopic = null,
            userTopic = null,
            userId = null;

    private AppState() {}

    public static AppState getInstance() {
        if (instance == null) {
            instance = new AppState();
        }

        return instance;
    }

    public boolean isConnectedToSomebody() {
        return connectedToSomebody;
    }

    public void setConnectedToSomebody(boolean connectedToSomebody) {
        this.connectedToSomebody = connectedToSomebody;
    }

    public boolean isConnectingToSomebody() {
        return connectingToSomebody;
    }

    public void setConnectingToSomebody(boolean connectingToSomebody) {
        this.connectingToSomebody = connectingToSomebody;
    }

    public boolean isStrangerDisconnected() {
        return strangerDisconnected;
    }

    public void setStrangerDisconnected(boolean strangerDisconnected) {
        this.strangerDisconnected = strangerDisconnected;
    }

    public boolean isStrangerIsTyping() {
        return strangerIsTyping;
    }

    public void setStrangerIsTyping(boolean strangerIsTyping) {
        this.strangerIsTyping = strangerIsTyping;
    }

    public boolean isUserDisconnected() {
        return userDisconnected;
    }

    public void setUserDisconnected(boolean userDisconnected) {
        this.userDisconnected = userDisconnected;
    }

    public String getStrangerTopic() {
        return strangerTopic;
    }

    public void setStrangerTopic(String strangerTopic) {
        this.strangerTopic = strangerTopic;
    }

    public String getUserTopic() {
        return userTopic;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        this.userTopic = CLIENT_TOPIC + userId;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
