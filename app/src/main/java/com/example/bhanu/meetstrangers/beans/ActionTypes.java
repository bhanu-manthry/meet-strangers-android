package com.example.bhanu.meetstrangers.beans;

/**
 * Created by bhanu on 1/11/17.
 */

public class ActionTypes {
    public static final String NEW_USER = "NEW_USER";
    public static final String USER_TRYING_TO_CONNECT = "USER_TRYING_TO_CONNECT";
    public static final String CONNECT_PING_REQ = "CONNECT_PING_REQ";
    public static final String CONNECT_PING_ACK = "CONNECT_PING_ACK";
    public static final String USER_GONE_AWAY = "USER_GONE_AWAY";
    public static final String ON_MESSAGE_ARRIVED = "ON_MESSAGE_ARRIVED";
    public static final String ON_STRANGER_DISCONNECT = "ON_STRANGER_DISCONNECT";
    public static final String GET_ONLINE_USERS = "GET_ONLINE_USERS";
    public static final String ON_STRANGER_IS_TYPING = "ON_STRANGER_IS_TYPING";
    public static final String ON_UPDATE_ONLINE_USERS = "ON_UPDATE_ONLINE_USERS";
    public static final String MESSAGE_SENT_SUCCESS = "MESSAGE_SENT_SUCCESS";
    public static final String MESSAGE_SENT_ERROR = "MESSAGE_SENT_ERROR";

}
