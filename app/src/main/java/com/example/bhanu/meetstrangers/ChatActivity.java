package com.example.bhanu.meetstrangers;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.bhanu.meetstrangers.beans.ActionTypes;
import com.example.bhanu.meetstrangers.beans.ChatMessage;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;

public class ChatActivity extends AppCompatActivity implements MessagesListener {

    private static final String TAG = ChatActivity.class.getSimpleName();
    FloatingActionButton sendBtn;
    EditText msgEditText;
    ListView messagesListView;
    MessagesAdapter messagesAdapter;
    MediaPlayer mediaPlayer;
    Vibrator vibe;

    MqttWorker mqttWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Meet Strangers");
        setSupportActionBar(toolbar);

        vibe = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.gentle_alarm);

        mqttWorker = MqttWorker.getInstance(getApplicationContext());

        sendBtn = findViewById(R.id.button_chatbox_send);
        sendBtn.setImageDrawable(getResources().getDrawable(R.drawable.ic_send));

        msgEditText = findViewById(R.id.edittext_chatbox);

        messagesListView = findViewById(R.id.messages_listview);
        messagesListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        messagesListView.setStackFromBottom(true);

        messagesAdapter = new MessagesAdapter(getApplicationContext());
        messagesListView.setAdapter(messagesAdapter);

        sendBtn.setOnClickListener((view) -> {

            String msgStr = msgEditText.getText().toString();

            if (msgStr.trim().length() == 0) {
                return;
            }

            String temp[] = AppState.getInstance().getStrangerTopic().split("/");
            String toId = temp[temp.length - 1];
            temp = AppState.getInstance().getUserTopic().split("/");
            String fromId = temp[temp.length - 1];

            ChatMessage chatMessage = new ChatMessage(msgStr, null, "sent", toId, fromId);
            messagesAdapter.newMessage(chatMessage);
            msgEditText.setText("");

            ChatMessage sendMsgType = new ChatMessage(msgStr, null, "received", toId, fromId);
            sendMsgType.getMessage().setType("received");
            SendMessage.getInstance(getApplicationContext()).sendChatMessage(AppState.getInstance().getStrangerTopic(), sendMsgType);
        });


        mqttWorker.setListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.disconnectBtn:
                if (item.getTitle().toString().equalsIgnoreCase("disconnect")) {
                    item.setTitle("sure ?");
                    break;
                }
                Log.d(TAG, "disconnect pressed");
                SendMessage.getInstance(getApplicationContext()).disconnectStranger();

                ChatMessages.getInstance().deleteAllMessages();
                messagesAdapter.notifyDataSetChanged();

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMessageArrival(String msgStr) throws JSONException {
        Log.d(TAG, "oMessageArrival: " + msgStr);
        JsonParser parser = new JsonParser();
        Gson gson = new Gson();
        JsonObject jsonObject = parser.parse(msgStr).getAsJsonObject();
        String type = jsonObject.get("notification_type").getAsString();

        Log.d(TAG, "type: " + type);

        if (type.equals(ActionTypes.ON_MESSAGE_ARRIVED)) {
            Log.d(TAG, "trace IN IF: " + type);
            Log.d(TAG, "trace after removing: " + type);
            ChatMessage msg = gson.fromJson(jsonObject.toString(), ChatMessage.class);
            messagesAdapter.newMessage(msg);
            mediaPlayer.start();
        }

        if (type.equals(ActionTypes.ON_STRANGER_DISCONNECT)) {
            disconnectStranger();
        }

        if (type.equals(ActionTypes.USER_GONE_AWAY)) {
            Log.d(TAG, "in user gone away");
            String strangerId = jsonObject.get("strangerId").getAsString();
            String strangerTopic = jsonObject.get("strangerTopic").getAsString();
            if (AppState.getInstance().getStrangerTopic().equals(strangerTopic)) {
                Log.d(TAG, "in user gone away next....");
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference onlineUsersRef = database.getReference("onlineUsers");
                onlineUsersRef.child(strangerId).removeValue();
                disconnectStranger();
            }
        }
    }

    void disconnectStranger() {
        Log.d(TAG, "stranger disconnected");

        AppState appState = AppState.getInstance();
        appState.setStrangerDisconnected(true);
        appState.setConnectedToSomebody(false);
        appState.setStrangerTopic(null);

        FirebaseLib.getInstance(getApplicationContext()).updateUserState(appState);
        ChatMessages.getInstance().deleteAllMessages();
        messagesAdapter.notifyDataSetChanged();

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
