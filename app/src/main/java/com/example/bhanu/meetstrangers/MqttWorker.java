package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.util.Log;

import com.example.bhanu.meetstrangers.beans.ActionTypes;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by bhanu on 1/11/17.
 */

public class MqttWorker {

    private static final String TAG = MqttWorker.class.getSimpleName();
    private static MqttWorker instance = null;
    private MessagesListener messagesListener = null;
    private boolean disconnectedManully = false;

    Context context;

    String brokerURI = "tcp://broker.hivemq.com:1883";
    MqttAndroidClient mqttClient;
    public ArrayList<String> subscriptionTopics = null;

    private MqttWorker(Context context) {
        this.context = context;
        subscriptionTopics = new ArrayList<>();
        Log.d(TAG, "Connecting to the borker for the first time...");
    }

    public static MqttWorker getInstance(Context context) {
        if (instance == null) {
            instance = new MqttWorker(context);
        }

        return instance;
    }


    public void connect(MainActivity.AfterMqttConnect afterMqttConnect) {
        if (mqttClient != null) {
            if (mqttClient.isConnected()) {
                Log.d(TAG, "Client is already connected to borker, no need to connect again.");
            }
        }

        String clientId = MqttClient.generateClientId();
        mqttClient =
                new MqttAndroidClient(context, brokerURI,
                        clientId);

        try {
            MqttConnectOptions auth = new MqttConnectOptions();
            auth.setKeepAliveInterval(10);

            JsonObject willMessage = new JsonObject();
            Gson gson = new Gson();
            willMessage.addProperty("notification_type", ActionTypes.USER_GONE_AWAY);
            willMessage.addProperty("strangerId", AppState.getInstance().getUserId());
            willMessage.addProperty("strangerTopic", AppState.getInstance().getUserTopic());
            auth.setWill(AppState.CLIENT_TOPIC, gson.toJson(willMessage).getBytes(), 1, false);
            IMqttToken token = mqttClient.connect(auth);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d(TAG, "onSuccess");

                    if (subscriptionTopics.size() == 0) {
                        Log.d(TAG, "No initial topics to subscribe.");
                        return;
                    }

                    for (String t : subscriptionTopics) {
                        subscribe(t);
                    }

                    afterMqttConnect.callback(true);

//                    SendMessage.getInstance(context).newUser();
//                    SendMessage.getInstance(context).getOnlineUsers();

                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems

                    Log.d(TAG, "onFailure");
                    afterMqttConnect.callback(false);
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
            afterMqttConnect.callback(false);
        }

        mqttClient.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable cause) {
                Log.e(TAG, "Mqtt connection lost");

                if (!disconnectedManully) {
                    Log.d(TAG, "disconnectedManully: " + disconnectedManully);
                    Log.d(TAG, "Trying to reconnect...");
                    connect(connected -> {});
                }
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                String receivedMsgString = new String(message.getPayload());
                messagesListener.onMessageArrival(receivedMsgString);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }

    public void setListener(MessagesListener messagesListener) {
        this.messagesListener = messagesListener;
    }

    public void publishMqttMessage(String topic, String payload) {
        Log.d(TAG, "publishing message: " +  payload);
        byte[] encodedPayload;
        try {
            encodedPayload = payload.getBytes("UTF-8");
            MqttMessage message = new MqttMessage(encodedPayload);
            mqttClient.publish(topic, message);
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    public void subscribe(String topic) {
        int qos = 1;
        try {
            IMqttToken subToken = mqttClient.subscribe(topic, qos);
            subToken.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d(TAG, "subscribed to topic: " + topic);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    // The subscription could not be performed, maybe the user was not
                    // authorized to subscribe on the specified topic e.g. using wildcards
                    Log.d(TAG, "subscription could not be performed due to unknown error, topic: " + topic);
                }
            });
        } catch (MqttException e) {
            Log.d(TAG, "subscription failed (topic: " + topic + ")");
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.d(TAG, "subscription failed (topic: " + topic + ") ... NullPointerException");
            e.printStackTrace();
        }
    }
}
