package com.example.bhanu.meetstrangers;

import android.content.Context;
import android.util.Log;

import com.example.bhanu.meetstrangers.beans.ActionTypes;
import com.example.bhanu.meetstrangers.beans.ChatMessage;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;

/**
 * Created by bhanu on 1/11/17.
 */

class SendMessage {
    private static final String TAG = SendMessage.class.getSimpleName();
    private static SendMessage instance = null;
    private MqttWorker mqttWorker;
    private Gson gson = new Gson();

    private SendMessage(Context context) {
        mqttWorker = MqttWorker.getInstance(context);
    }

    static SendMessage getInstance(Context context) {
        if (instance == null) {
            instance = new SendMessage(context);
        }

        return instance;
    }

    void newUser() {
        AppState appState = AppState.getInstance();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.NEW_USER);
        jsonObject.add("updatedObj", gson.toJsonTree(appState));
        mqttWorker.publishMqttMessage(AppState.CLIENT_STATUS_TOPIC, gson.toJson(jsonObject));
    }

    void getOnlineUsers() {
        AppState appState = AppState.getInstance();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.GET_ONLINE_USERS);
        jsonObject.add("updatedObj", gson.toJsonTree(appState));
        mqttWorker.publishMqttMessage(AppState.CLIENT_STATUS_TOPIC, gson.toJson(jsonObject));
    }

    void userTryingToConnect() {
        Log.d(TAG, "user trying to connect...");
        AppState appState = AppState.getInstance();
        appState.setConnectingToSomebody(true);
        ChatMessages.getInstance().setMessages(new ArrayList<>());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.USER_TRYING_TO_CONNECT);
        jsonObject.add("updatedObj", gson.toJsonTree(appState));
        mqttWorker.publishMqttMessage(AppState.CLIENT_STATUS_TOPIC, gson.toJson(jsonObject));
    }

    void disconnectStranger() {
        AppState appState = AppState.getInstance();
        appState.setConnectingToSomebody(false);
        appState.setConnectedToSomebody(false);
        appState.setUserDisconnected(true);

        // this object is sent to stranger
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.ON_STRANGER_DISCONNECT);
        JsonObject updatedObj = new JsonObject();
        updatedObj.addProperty("strangerDisconnected", true);
        updatedObj.addProperty("connectedToSomebody", false);
        jsonObject.add("updatedObj", gson.toJsonTree(updatedObj));
        mqttWorker.publishMqttMessage(appState.getStrangerTopic(), gson.toJson(jsonObject));

        appState.setStrangerTopic("");
    }

    void sendPingReq(String topic) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.CONNECT_PING_REQ);
        jsonObject.addProperty("strangerTopic", AppState.getInstance().getUserTopic());

        String jsonStr = gson.toJson(jsonObject);
        mqttWorker.publishMqttMessage(topic, jsonStr);
    }

    void sendPingAck(String topic) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.CONNECT_PING_ACK);
        jsonObject.addProperty("strangerTopic", AppState.getInstance().getUserTopic());

        String jsonStr = gson.toJson(jsonObject);
        mqttWorker.publishMqttMessage(topic, jsonStr);
    }

    void sendChatMessage(String topic, ChatMessage chatMessage) {
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(gson.toJson(chatMessage)).getAsJsonObject();
        jsonObject.addProperty("notification_type", ActionTypes.ON_MESSAGE_ARRIVED);

        String jsonStr = gson.toJson(jsonObject);
        mqttWorker.publishMqttMessage(topic, jsonStr);
    }
}
