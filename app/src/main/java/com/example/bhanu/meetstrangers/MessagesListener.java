package com.example.bhanu.meetstrangers;

import org.json.JSONException;

/**
 * Created by bhanu on 1/11/17.
 */

public interface MessagesListener {
    void onMessageArrival(String msgString) throws JSONException;
}
